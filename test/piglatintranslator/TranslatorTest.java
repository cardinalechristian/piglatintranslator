package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void inputPhraseTest() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	
	@Test
	public void translationEmptyPhraseTest() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	
	@Test
	public void translationPhraseStartingWithAEndingWithYTest() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	
	@Test
	public void translationPhraseStartingWithUEndingWithYTest() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	
	@Test
	public void translationPhraseStartingWithVowelEndingWithVowelTest() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void translationPhraseStartingWithVowelEndingWithConsonantTest() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	
	@Test
	public void translationPhraseStartingWithSingleConsonantTest() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	
	@Test
	public void translationPhraseStartingWithMoreConsonantTest() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	
	@Test
	public void translationPhraseWithMoreWordsTest() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	
	@Test
	public void translationPhraseContainingPunctuationsTest() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	
	@Test
	public void translationPhraseContainingNotAllowedCharactersTest() {
		String inputPhrase = "hello world&";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.INCORRECT, translator.translate());
	}

	

}
