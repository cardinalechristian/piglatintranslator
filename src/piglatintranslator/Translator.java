package piglatintranslator;

import java.util.ArrayList;

public class Translator {

	public static final String NIL = "nil";
	public static final String INCORRECT = "incorrect input phrase";
	private String phrase;
	private char punctuation[] = {'.',',',';',':','?','!','(',')','�'};
	private char consonants[] = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z'};
	private ArrayList<Character> whitespace_dash = new ArrayList<Character>();
	private ArrayList<String> translated_words = new ArrayList<String>(); 
	private String translated_phrase;

	
	
	public Translator(String inputPhrase) {
		this.phrase = inputPhrase;
	}

	
	public String getPhrase() {
		return phrase;
	}

	
	
	public String translationRules(String word) {
		
		if(startWithVowel(word)) {	
			
			if(word.endsWith("y")) {
				word = word + "nay"; 
			}else if(endWithVowel(word)) {
				word = word + "yay"; 
			}else if(!endWithVowel(word)) {
				word = word + "ay"; 
			}
		
		}else if(startWithConsonant(word)) {
	
			if(startWithSingleConsonant(word)) {
				word = moveFirstConsonantToTheEndOfString(word) + "ay"; 
			}else{
				word = moveStartingConsonantsToTheEndOfString(word) + "ay"; 
			}
		
		}
		
		return word;
	}
	
	
	
	public String translate() {
		
		if(phrase.isEmpty()) {
				return NIL;
		}else if(!charactersAccepted()) {
				return INCORRECT;
		}
		
		
		String words[] = phrase.split(" |\\-");
		
		
		for(String word : words) {
				
				boolean start = false;
				boolean end = false;
				char first_punctuation = ' ';
				char last_punctuation = ' ';
				
				if(word.equals(".") || word.equals(",") || word.equals(";") || word.equals(":") || word.equals("?") || word.equals("!") || word.equals("(") || word.equals(")") || word.equals("�")) {
					
					translated_words.add(word); 
					
				}else {
					
						for(int i = 0 ; i < word.length() ; i++) {
							for(int j = 0; j < punctuation.length ; j++) {
								if(word.charAt(i) == punctuation[j] && i == 0) {
									first_punctuation = word.charAt(i);
									start = true;
								}else if(word.charAt(i) == punctuation[j] && i == word.length()-1) {
									last_punctuation = word.charAt(i);
									end = true;
								}
							}
						
						}
					
					
						if(start && end) {
							translated_words.add(first_punctuation + translationRules(word.substring(1,word.length()-1)) + last_punctuation );
						}else if (start) {
							translated_words.add(first_punctuation + translationRules(word.substring(1)) );
						}else if (end) {
							translated_words.add(translationRules(word.substring(0,word.length()-1)) + last_punctuation );
						}else {
							translated_words.add(translationRules(word));
						}
					
					
				}
		
		}
		
		
		for(int i = 0 ; i < phrase.length() ; i++) {
			if(phrase.charAt(i) == ' ') {
				whitespace_dash.add(' ');
			}else if(phrase.charAt(i) == '-') {
				whitespace_dash.add('-');
			}
		}
		
		if(whitespace_dash.isEmpty()) {
			whitespace_dash.add('e');
		}
		
			
			for (int i = 0; i < translated_words.size(); i++) {
				if(i == 0 && whitespace_dash.get(0) == 'e') {											
					translated_phrase = translated_words.get(0);
				}else if(i == 0 && whitespace_dash.get(0) == ' ') {											
					translated_phrase = translated_words.get(0) + " ";
				}else if(i == 0 && whitespace_dash.get(0) == '-') {											
					translated_phrase = translated_words.get(0) + "-";
				}else if(i == translated_words.size()-1){																		
					translated_phrase = translated_phrase + translated_words.get(i);
				}else if(whitespace_dash.get(i) == ' '){																		
					translated_phrase = translated_phrase + translated_words.get(i) + " ";
				}else if(whitespace_dash.get(i) == '-'){									
					translated_phrase = translated_phrase + translated_words.get(i) + "-";
				}
			}
		
		
		
		
		return translated_phrase;
		
	}
	
	
	private boolean charactersAccepted() {
		
		for(int i = 0; i < phrase.length(); i++) {
			
			boolean isVowel = true;
			boolean isConsonant = true;
			boolean isPunctuation = true;
			boolean isWhiteSpaceOrDash = true;
			
			if( !(phrase.charAt(i) == 'a' || phrase.charAt(i) == 'e' || phrase.charAt(i) == 'i' || phrase.charAt(i) == 'o' || phrase.charAt(i) == 'u') ){
				isVowel = false;
			}
			
			for(int j = 0 ; j < consonants.length ; j++) {
				if(phrase.charAt(i) != consonants[j]) {
					isConsonant = false;
				}else {
					isConsonant = true;	
					break;
				}
			}
			
			for(int k = 0 ; k < punctuation.length ; k++) {
				if(phrase.charAt(i) != punctuation[k]) {
					isPunctuation = false;
				}else {
					isPunctuation = true;	
					break;
				}
			}
			
			if( !(phrase.charAt(i) == ' ' || phrase.charAt(i) == '-') ){
				isWhiteSpaceOrDash = false;
			}
			
			if(!isVowel && !isConsonant && !isPunctuation && !isWhiteSpaceOrDash ) {
				return false;
			}
		}
		
		return true;
	}


	private boolean startWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u") ;
	}
	

	private boolean endWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u") ;
	}
	
	private boolean startWithConsonant(String word) {
		return !(word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u")) ;
	}

	private boolean startWithSingleConsonant(String word) {
		return word.charAt(1) == 'a' || word.charAt(1) == 'e' || word.charAt(1) == 'i' || word.charAt(1) == 'o' || word.charAt(1) == 'u';
	
	}
	
	
	
	private String moveFirstConsonantToTheEndOfString(String word) {
		
		word = word.substring(1) + word.charAt(0);
		
		return word;
	}
	
	
	private String moveStartingConsonantsToTheEndOfString(String word) {
		
		int num_starting_consonants = 0;
		
		for(int i = 0 ; i < word.length() ; i++) {
			
			if(!(word.charAt(i) == 'a' || word.charAt(i) == 'e' || word.charAt(i) == 'i' || word.charAt(i) == 'o' || word.charAt(i) == 'u')) {
					num_starting_consonants++;
			}else {
					break;
			}
		}
		
		word = word.substring(num_starting_consonants, word.length()) + word.substring(0, num_starting_consonants);
		
		return word;
	}
	
	
	
}
